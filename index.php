<?php
require_once("inc/ZabbixAPI.class.php");
include("config.inc.php");

/*
include("config.inc.php");
include("inc/index.functions.php");
include("config.php");
*/

if ( $user_login == 0 ) {
  header("Location: chooser.php");
  exit(0);
}

session_start();
if($_SERVER["REQUEST_METHOD"] == "POST")
{
// username and password sent from Form
$myusername=addslashes($_POST['username']);
$mypassword=addslashes($_POST['password']);

//session_register("myusername");
$_SESSION['login_user']=$myusername;
$_SESSION['username']=$myusername;
$_SESSION['password']=$mypassword;
//print_r($_SESSION); 

if ( $zabbix_version < 5.0 ) { 
  ZabbixAPI::debugEnabled(TRUE);
}

ZabbixAPI::login($z_server,$myusername,$mypassword)
	or die('Unable to login: '.print_r(ZabbixAPI::getLastError(),true));

header("location: chooser.php");
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
	<title>Layer37NMS Dynamic PDF Report</title>
	<meta charset="utf-8" />
	<link rel="shortcut icon" href="/zabbix/favicon.ico" />
	<!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/zabbix.default.css" />
	<link rel="stylesheet" type="text/css" href="css/zabbix.color.css" />
	<link rel="stylesheet" type="text/css" href="css/zabbix.report.css" />
	<link rel="stylesheet" type="text/css" href="css/tablesorter.css"/ >
	<link rel="stylesheet" type="text/css" href="css/select2.css"/ >
	
</head>
<body class="originalblue">
<div id="message-global-wrap"><div id="message-global"></div></div>
<table class="maxwidth page_header" cellspacing="0" cellpadding="5"><tr><td class="page_header_l"><a class="image" href="" target="_blank"><div class="zabbix_logo">&nbsp;</div></a></td><td class="maxwidth page_header_r">&nbsp;</td></tr></table>
<br/><br/>
<center><h2 class="text-center">Log Into Layer37NMS To Generate PDF Reports</h2></center>
<br/>
<center>
<div class="login-form">
<form action="" method="post">
<div class="container">
<table class="table" border="1" rules="NONE" frame="BOX" width="250" cellpadding="15">
<tr><td valign="middle" align="right" width="115">
<label for="Username" class="h6"><b></b></label>
</td><td valign="center" align="center" height="30">
<br>
<p>
<input type="text" class="form-control" placeholder="Username" name="username" required /><br />
</p>
</td><td valign="middle" width="110">
&nbsp;
</td></tr>
<tr><td valign="middle" align="right" width="115">
<label for="Password" class="h6"><b></b></label>
</td><td valign="center" align="center" height="30">
<p>
<input type="password" class="form-control" placeholder="Password" name="password" required /><br />
</p>
</td><td valign="middle" width="110">
&nbsp;
</td></tr>
<td>&nbsp;</td><td  align="center">
<input class="btn btn-primary btn-block" type='submit' value='Sign in'>
<!--<p>Version <?php// echo($version); ?></p>-->
</td></tr>
</table>
</div>
<!--
<label>UserName :</label>
<label>Password :</label>
<input type="submit" value=" Submit "/><br />
-->
</form>
<div>
